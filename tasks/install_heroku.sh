#!/usr/bin/env bash

set -e

set -x

base_path="tools"
package_name="heroku.tar.gz"

if [ ! -d "$base_path" ]
then
  mkdir $base_path
fi

if [ ! -f "$base_path/$package_name" ]
then
  wget -nv -O $base_path/$package_name https://cli-assets.heroku.com/branches/stable/heroku-linux-amd64.tar.gz
fi

sudo mkdir -p /usr/local/lib /usr/local/bin
sudo tar -xzf $base_path/$package_name -C /usr/local/lib
sudo ln -s /usr/local/lib/heroku/bin/heroku /usr/local/bin/heroku
