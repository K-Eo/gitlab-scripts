#!/usr/bin/env bash

set -e

set -x

gem install bundler --no-ri --no-doc

bundle install --jobs $(nproc) --path vendor/bundle
