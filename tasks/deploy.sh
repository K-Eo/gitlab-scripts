#!/usr/bin/env bash

function handle_error {
  echo Disabling maintenance.
  heroku maintenance:off --app $APP_NAME
  exit 1
}

trap handle_error ERR

set -e

which ssh-agent
eval $(ssh-agent -s)
echo "$SSH_HEROKU_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
mkdir -p ~/.ssh
chmod 700 ~/.ssh

echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

cat > ~/.netrc << EOF
machine api.heroku.com
  login $HEROKU_LOGIN
  password $HEROKU_API_KEY
EOF

set -x

if git remote | grep -q "heroku"
then
  git remote remove heroku
fi

git remote add heroku git@heroku.com:$APP_NAME.git
git fetch heroku

MIGRATION_CHANGES=$(git diff HEAD heroku/master --name-only -- db | wc -l)

PREV_WORKERS=$(heroku ps --app $APP_NAME | grep "^worker." | wc -l | tr -d ' ')

if test $MIGRATION_CHANGES -gt 0; then
  heroku maintenance:on --app $APP_NAME
  heroku scale worker=0 --app $APP_NAME
fi

git push heroku $CI_COMMIT_SHA:refs/heads/master

if test $MIGRATION_CHANGES -gt 0; then
  heroku run rake db:migrate --app $APP_NAME
  heroku scale worker=$PREV_WORKERS --app $APP_NAME
  heroku restart --app $APP_NAME
  heroku maintenance:off --app $APP_NAME
fi
